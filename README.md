# client

> Nuxt.js + Vuetify.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [Nuxt.js](https://github.com/nuxt/nuxt.js) and [Vuetify.js](https://vuetifyjs.com/) documentation.


## Add locale

1. Create locale json under locales folder

2. Import file in i18n plugin file

3. Add locale to store initial state

## Add page

1. Create page file in lang folder

2. Create page in root and import lang page

3. Create page object in locales

4. Add page to layout urls
