export default ({app, store}, inject) => {
  inject('globals', {
    hasNativeInputs () {
      // is chrome browser
      const isDesktopChrome = store.state.userAgent.match(/Chrome\/[.0-9]* (?!Mobile)/i)
      const isEdge = store.state.userAgent.match(/Edge\/[.0-9]*/i)
      if (isEdge) {
        return true
      } else if (isDesktopChrome) {
        return false
      }
      return true
    }
  })
}
