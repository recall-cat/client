import Echo from 'laravel-echo'
import io from 'socket.io-client'

window.io = io

export default ({store}, inject) => {
  const client = new Echo({
    broadcaster: 'socket.io',
    host: process.env.WS_URL || window.location.host,
    auth: {
      headers: {
        Authorization: 'Bearer ' + store.state.auth.token
      }
    }
  })
  inject('echo', client)
}
