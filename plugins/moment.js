import moment from 'moment'

export default ({app, store}, inject) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  moment.locale(store.state.locale)
  app.$moment = moment
  inject('moment', moment)
}
