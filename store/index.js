import moment from 'moment'

export const state = () => ({
  locales: ['en', 'es'],
  locale: 'en',
  userAgent: ''
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      moment.locale(locale)
      state.locale = locale
    }
  },
  SET_USER_AGENT (state, userAgent) {
    state.userAgent = userAgent
  }
}

export const actions = {
  async nuxtServerInit ({commit, dispatch, state}, {req, params, app: {i18n}}) {
    i18n.locale = params.lang || 'en'
    if (req.path === '/login-confirm') return
    if (req.path === '/claim-confirm') return
    if (req.session && req.session.auth) {
      try {
        const data = await dispatch('auth/reload')
        commit('auth/login', req.session.auth.token)
        req.session.auth.email = data.email
        req.session.auth.id = data.id
      } catch (e) {
        console.log(e.message)
        await dispatch('auth/registerLocal')
      }
    } else {
      if (req.path !== i18n.path('')) {
        await dispatch('auth/registerLocal')
      }
    }
  }
}
