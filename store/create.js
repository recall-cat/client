// const today = () => {
//   return new Date().toJSON().slice(0, 10)
// }
// const now = () => {
//   return new Date().toJSON().slice(11, 17) + '00'
// }

import moment from 'moment'

const defaultNotificationState = {
  notification: {
    browser: true,
    mail: false
  }
}

export const state = () => ({
  date: null,
  time: null,
  url: '',
  message: '',
  headline: '',
  loading: false,
  data: null,
  privacy: 'private',
  advanced: false,
  ...defaultNotificationState
})

export const mutations = {
  updateDate (state, value) {
    state.date = value
  },
  updateTime (state, value) {
    state.time = value
  },
  updateMessage (state, value) {
    state.message = value
  },
  updatePrivacy (state, value) {
    state.privacy = value
  },
  updateHeadline (state, value) {
    state.headline = value
  },
  updateUrl (state, value) {
    state.url = value
  },
  updateNotification (state, {notification, value}) {
    state.notification[notification] = value
  },
  updateAdvanced (state, value) {
    state.advanced = value
  },
  setLoading (state, value) {
    state.loading = value
  },
  setData (state, value) {
    state.data = value
  },
  clearForm (state) {
    state.date = null
    state.time = null
    state.headline = ''
    state.message = ''
    state.url = ''
    state.privacy = 'private'
    state = {
      ...state,
      ...defaultNotificationState
    }
  }
}

export const actions = {
  async submit ({state, commit, rootState, dispatch}) {
    commit('setLoading', true)
    try {
      if (!rootState.auth.authenticated) {
        await dispatch('auth/registerLocal', null, {root: true})
      }
      let {data} = await this.$axios.post('/reminders', {
        datetime: moment(`${state.date} ${state.time}`).format(),
        message: state.message,
        url: state.url,
        headline: state.headline,
        notification: state.notification,
        privacy: state.privacy
      })
      commit('setData', data.data)
      commit('clearForm')
      commit('alert/open', {
        message: 'page.create.alert.success',
        action: {
          text: 'actions.view',
          path: `reminders/${data.data.hash}/${data.data.slug}`
        }
      }, {root: true})
      dispatch('feed/fetchReminders', null, {root: true})
    } catch (e) {
      commit('alert/open', {
        color: 'error',
        message: 'page.create.alert.error'
      }, {root: true})
      console.log(e)
    } finally {
      commit('setLoading', false)
    }
  }
}
