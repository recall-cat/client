export const state = () => ({
  authenticated: false,
  token: '',
  vapidPublicKey: null,
  email: null,
  id: null
})

export const mutations = {
  login (state, token) {
    state.authenticated = true
    state.token = token
    this.$axios.setToken(token, 'Bearer')
  },
  email (state, email) {
    state.email = email
  },
  id (state, id) {
    state.id = id
  },
  logout (state) {
    state.authenticated = false
    state.token = ''
    state.email = null
    state.id = null
    this.$axios.setToken(null)
  },
  vapid (state, publicKey) {
    state.vapidPublicKey = publicKey
  }
}

const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3000

const baseURL = process.browser ? window.location.origin : `http://${host + ':' + port}`

export const actions = {
  async login ({commit, dispatch}, token) {
    try {
      const data = await this.$axios.$post(baseURL + '/auth/login', {
        token
      })
      commit('login', token)
      commit('email', data.email)
      commit('id', data.id)
    } catch (e) {
      console.log(e)
    }
  },
  async logout ({commit}) {
    try {
      await this.$axios.post(baseURL + '/auth/logout')
      commit('logout')
    } catch (e) {
      console.log(e)
    }
  },
  async reload ({commit}) {
    try {
      const data = await this.$axios.$post(baseURL + '/auth/reload')
      commit('email', data.email)
      commit('id', data.id)
      return data
    } catch (e) {
      console.log(e)
    }
  },
  async registerLocal ({commit}) {
    const data = await this.$axios.$post(baseURL + '/auth/register', {
      type: 'local'
    })
    commit('login', data.token)
    commit('email', data.email)
    commit('id', data.id)
    return data
  },
  async getVapidKeys ({commit}) {
    const {data} = await this.$axios.get('/vapid')
    commit('vapid', data.public_key)
  },
  async subscriptionStatus ({commit}) {
    const {data} = await this.$axios.get('/subscription')
    commit('subscribed', data.subscribed)
  }
}
