export const state = () => ({
  // can use push notifications
  available: false,
  // its server subscribed
  subscribed: false,
  // user blocked notifications,
  denied: false,
  // push notifications are active
  granted: false,
  // checked the browser
  detected: false
})

export const mutations = {
  subscribed (state, value) {
    state.subscribed = value
  },
  available (state, value) {
    state.available = value
    state.detected = true
  },
  denied (state, value) {
    state.denied = value
  },
  granted (state, value) {
    state.granted = value
  }
}
