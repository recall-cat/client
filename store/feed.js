export const state = () => ({
  reminders: [],
  followedReminders: []
})

export const mutations = {
  setReminders (state, reminders) {
    state.reminders = reminders
  },
  setFollowedReminders (state, reminders) {
    state.followedReminders = reminders
  }
}

export const actions = {
  async fetchReminders ({commit}) {
    try {
      let {data} = await this.$axios.get('/reminders')
      // say it quickly, its funny
      commit('setReminders', data.data)
    } catch (e) {
      console.log(e)
    } finally {
      //
    }
  },
  async fetchFollowedReminders ({commit}) {
    let {data} = await this.$axios.get('/reminders/followed')
    // say it quickly, its funny
    commit('setFollowedReminders', data.data)
  }
}
