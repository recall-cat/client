export const state = () => ({
  messages: [],
  message: '',
  color: '',
  timeout: 0,
  open: false,
  actionText: '',
  actionPath: ''
})

export const mutations = {
  open (state, {message, color = 'success', timeout = 5000, action = null}) {
    state.message = message
    state.color = color
    state.timeout = timeout
    state.open = true
    if (action) {
      state.actionText = action.text
      state.actionPath = action.path
    } else {
      state.actionText = ''
      state.actionPath = ''
    }
  },
  close (state) {
    state.open = false
  }
}
