require('dotenv').config()

const nodeExternals = require('webpack-node-externals')

module.exports = {
  workbox: {
    importScripts: [
      'webpush-sw.js'
    ]
  },
  env: {
    WS_URL: process.env.WS_URL,
    SERVER_API_URL: process.env.SERVER_API_URL
  },
  axios: {
    baseURL: process.env.SERVER_API_URL + process.env.PREFIX,
    browserBaseURL: process.env.BROWSER_API_URL + process.env.PREFIX
  },
  sentry: {
    dsn: 'https://cca1c6b56233446b95cf3f5f6e7252e0@sentry.io/266094'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Recall Cat',
    meta: [
      {charset: 'utf-8'},
      {name: 'theme-color', content: '#007b0f'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {
        hid: 'description',
        name: 'description',
        content: 'Remind you with almost any channel and share it with your friends'
      }
    ],
    link: [
      {rel: 'icon', type: 'image/png', href: '/favicon.png'},
      {rel: 'manifest', href: '/manifest.json'}
    ]
  },
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/sentry',
    '@nuxtjs/axios',
    ['@nuxtjs/pwa', {icon: false, manifest: false, meta: false}]
  ],
  plugins: [
    '~/plugins/vuetify.js',
    '~/plugins/i18n.js',
    '~/plugins/moment.js',
    '~/plugins/globals.js',
    {src: '~/plugins/echo.js', ssr: false}
  ],
  router: {
    middleware: 'i18n'
  },
  css: [
    '~/assets/style/icons.scss',
    '~/assets/style/fonts.scss',
    '~/assets/style/base.scss',
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#007b0f'},
  /*
  ** Build configuration
  */
  build: {
    babel: {
      plugins: [
        ['transform-imports', {
          'vuetify': {
            'transform': 'vuetify/es5/components/${member}',
            'preventFullImport': true
          }
        }]
      ]
    },
    vendor: [
      '~/plugins/vuetify.js',
      'vue-i18n',
      'moment'
    ],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
      if (!ctx.isClient) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}
