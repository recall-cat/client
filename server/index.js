import express from 'express'
import { Nuxt, Builder } from 'nuxt'
import Session from 'express-session'
import ConnectRedis from 'connect-redis'

import auth from './auth'

const app = express()
const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3000

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

app.set('port', port)

const RedisStore = ConnectRedis(Session)
// Session config
const sessionConfig = {
  secret: '-+/+*-+/-*-/*/*+*/',
  sameSite: true,
  resave: false,
  proxy: true,
  saveUninitialized: false,
  rolling: true,
  cookie: {maxAge: 30 * 24 * 60 * 60 * 1000},
  store: new RedisStore({
    prefix: 'nuxt-session:'
  })
}

if (!config.dev) {
  app.set('trust proxy', 1) // trust first proxy
  sessionConfig.cookie.secure = true // serve secure cookies
}

app.use(Session(sessionConfig))

// Import API Routes
app.use('/auth', auth)

// Init Nuxt.js
const nuxt = new Nuxt(config)

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

// Give nuxt middleware to express
app.use(nuxt.render)

// Listen the server
app.listen(port, host)

console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
