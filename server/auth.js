import { Router } from 'express'
import BodyParser from 'body-parser'
import axios from 'axios'

require('dotenv').config()

axios.defaults.baseURL = process.env.SERVER_API_URL

const router = Router()

// parse application/json
router.use(BodyParser.json())

router.post('/reload', (req, res) => {
  axios.post('/api/me', null, {
    headers: {
      'Authorization': `Bearer ${req.session.auth.token}`
    }
  })
    .then(({data}) => {
      res.json(data)
    })
    .catch((e) => {
      console.log(e.message)
      res.status(503).send()
    })
})

router.post('/register', (req, res) => {
  axios.post('/api/register', req.body)
    .then(({data}) => {
      req.session.auth = {
        token: data.token,
        email: data.email,
        id: data.id
      }
      res.json(req.session.auth)
    })
    .catch((e) => {
      console.log(e.message)
      res.status(503).send()
    })
})

router.post('/login', (req, res) => {
  axios.post('/api/me', null, {
    headers: {
      'Authorization': `Bearer ${req.body.token}`
    }
  })
    .then(({data}) => {
      req.session.auth = {
        token: req.body.token,
        email: data.email
      }
      res.json(data)
    })
    .catch((e) => {
      console.log(e.message)
      res.status(503).send()
    })
})

router.post('/logout', (req, res) => {
  req.session.destroy()
  res.status(200).send()
})

if (process.env.NODE_ENV !== 'production') {
  router.get('/test', (req, res) => {
    res.json(req.session)
  })
}

export default router
